package com.company.cracking;

import java.util.HashMap;
import java.util.Map;

public class Question1 {

    public static void main(String[] args) {
        System.out.println(isUniqueChars("Kanan"));
    }

    public static boolean isUniqueChars(String str) {
        boolean[] chars = new boolean[128];
        for (int i = 0; i < str.length(); i++) {
            int val = str.charAt(i);
            if (chars[val]) return false;
            chars[val] = true;
        }

        return true;
    }

    public static boolean isUniqueChars2(String str) {

        Map<Character, Integer> map = new HashMap<>();

        char[] arr = str.toCharArray();
        for (int i=0; i<arr.length;i++){
            map.put(arr[i],map.getOrDefault(arr[i],0)+1);
        }

        System.out.println(map);
        for (char key:map.keySet()){
            if (map.get(key)>1)
                return false;
        }

        return true;
    }

}
