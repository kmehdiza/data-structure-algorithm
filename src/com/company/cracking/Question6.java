package com.company.cracking;

public class Question6 {

    public static void main(String[] args) {
        System.out.println(stringCompression("aabcccccaaa"));
    }

    public static String stringCompression(String s) {
        System.out.println(s.length());
        StringBuilder builder = new StringBuilder();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            count++;
            System.out.println("index" + i);
            if (i + 1 >= s.length() || s.charAt(i) != s.charAt(i + 1)) {
                builder.append(s.charAt(i)).append(count);
                count = 0;
            }
        }
        return builder.length() < s.length() ? s : builder.toString();
    }
}
