package com.company.cracking;

public class Question3 {

    public static void main(String[] args) {
        System.out.println(urLify2("Mr John Smith    "));
    }

    public static String urLify(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        char[] arr = s.trim().toCharArray();
        for (char letter : arr) {
            if (letter == ' ') {
                stringBuilder.append("%20");
            } else {
                stringBuilder.append(letter);
            }
        }
        return stringBuilder.toString();
    }


    public static String urLify2(String s) {
        char[] arr = s.trim().toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == ' ') {
                arr[i] = '2';
            }
        }
        return String.valueOf(arr);
    }

}
