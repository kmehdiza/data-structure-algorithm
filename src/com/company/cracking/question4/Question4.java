package com.company.cracking.question4;

public class Question4 {


    public static void main(String[] args) {
        boolean res = isPermutationOfPalindrome("Tact Coa");
        System.out.println(res);
    }

    public static boolean isPermutationOfPalindrome(String phrase) {
        int[] arr = buildCharFrequencyTable(phrase);
        return checkMaxOneOdd(arr);
    }

    public static int getCharNumber(Character c) {
        int a = Character.getNumericValue('a');
        int z = Character.getNumericValue('z');
        int val = Character.getNumericValue(c);

        if (a <= val && val <= z) {
            return val - a;
        }
        return -1;
    }

    public static int[] buildCharFrequencyTable(String phrase) {
        int[] table = new int[Character.getNumericValue('z') - Character.getNumericValue('a') + 1];
        for (char c : phrase.toCharArray()) {
            int x = getCharNumber(c);
            if (x != -1) {
                table[x]++;
            }
        }
        return table;
    }

    public static boolean checkMaxOneOdd(int[] table) {
        boolean foundOdd = false;
        for (int count : table) {
            if (count % 2 == 1) {
                if (foundOdd) {
                    return false;
                }
            }
            foundOdd = true;
        }
        return true;
    }
}
