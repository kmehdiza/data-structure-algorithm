package com.company.cracking;

public class Question2 {


    public static void main(String[] args) {
        permutation("kanan", "nanak");
    }

    public static boolean permutation(String s, String t) {

        int[] letters = new int[128];

        char[] arr = s.toCharArray();
        for (char c : arr) {
            letters[c]++;
        }

        for (int i = 0; i < t.length(); i++) {
            int val = t.charAt(i);
            System.out.println(val);
            if (--letters[val] < 0)
                return false;
        }
        return true;
    }

}
