package com.company.cracking;

public class Question5 {

    //Insert
    //Remove
    //Replace


    public static void main(String[] args) {
        String s = "pale";
        String t = "ple";
        boolean res = oneEditReplace(s, t);
        System.out.println(res);
    }

    public static boolean oneAway(String s1, String s2) {
        if (s2.length()==s1.length()){
            return oneEditReplace(s1,s2);
        }else if(s1.length()+1==s2.length()){
            return oneEditInsert(s1,s2);
        }else if(s1.length()-1==s2.length()){
            return oneEditInsert(s1,s2);
        }
        return false;
    }

    // ple
    // pale
    public static boolean oneEditInsert(String s1, String s2) {
        int index1 = 0;
        int index2 = 0;

        while (index2 < s2.length() && index1 < s1.length()) {
            if (s1.charAt(index1) != s2.charAt(index2)) {
                if (index1 != index2) {
                    return false;
                }
                index2++;
            }else {
                index1++;
                index2++;
            }
        }

        return true;
    }

    // pale
    // bale
    public static boolean oneEditReplace(String s, String t) {
        boolean foundReplace = false;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) != t.charAt(i)) {
                if (foundReplace) {
                    return false;
                }
                foundReplace = true;
            }
        }
        return true;
    }
}
