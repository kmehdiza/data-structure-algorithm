package com.company.cracking;

import java.util.HashMap;
import java.util.Map;

public class Question9 {


    public static void main(String[] args){
        System.out.println(stringRotation("waterbottle","erbottlewat"));
    }

    public static boolean stringRotation(String s1, String s2) {

        char[] arr1 = s1.toCharArray();
        char[] arr2 = s2.toCharArray();

        Map<Character, Integer> map = new HashMap<>();

        for (int i=0; i<arr1.length;i++){
            map.put(arr1[i],map.getOrDefault(arr1[i],0)+1);
            map.put(arr2[i],map.getOrDefault(arr2[i],0)-1);
        }
        System.out.println(map);

        for (char key: map.keySet()){
            if (map.get(key)!=0)
                return false;
        }
        return true;
    }

}
