package com.company.leetcode.string.jun032021;

public class Exercise1880 {

    public static void main(String[] args) {
        boolean res = isSumEqual("acb", "cba", "cdb");
        System.out.println(res);
    }

    public static boolean isSumEqual(String firstWord, String secondWord, String targetWord) {
        return getIntValue(firstWord) + getIntValue(secondWord) == getIntValue(targetWord);
    }

    private static int getIntValue(String word) {
        int num = 0;
        for (int i = 0; i < word.length(); i++) {
            System.out.println(word.charAt(i) - 'a');
            num = num * 10 + (word.charAt(i) - 'a');
        }
        return num;
    }
}
