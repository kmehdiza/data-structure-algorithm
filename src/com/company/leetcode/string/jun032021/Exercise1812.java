package com.company.leetcode.string.jun032021;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Exercise1812 {
    //{a=97, b=98, c=99, d=100, e=101, f=102, g=103, h=104}
    public static void main(String[] args) {
        boolean res = squareIsWhite2("c7");
        System.out.println(res);
    }

    public static boolean squareIsWhite(String coordinates) {

        Map<String, String> map = new HashMap<>();

        for (char i='a';i<='h';i++){
            int index = 1;
            while (index<=8){
                if ((int)i%2==1 && index%2==1){
                    map.put(i+""+index,"black");
                }else if ((int)i%2==1 && index%2==0){
                    map.put(i+""+index,"white");
                }else if ((int)i%2==0&&index%2==1){
                    map.put(i+""+index,"white");
                }else if ((int)i%2==0&&index%2==0){
                    map.put(i+""+index,"black");
                }
                index++;
            }
        }
        String color = map.get(coordinates);
        return color.equals("white");
    }

    public static boolean squareIsWhite2(String a) {
        System.out.println((int)a.charAt(0) % 2);
        System.out.println((int)a.charAt(1) % 2);
        return (int)a.charAt(0) % 2 != (int)a.charAt(1) % 2;
    }

}
