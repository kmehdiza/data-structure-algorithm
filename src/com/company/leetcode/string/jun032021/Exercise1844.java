package com.company.leetcode.string.jun032021;

import java.util.HashMap;
import java.util.Map;

public class Exercise1844 {

    public static void main(String[] args) {
        String res = replaceDigits("c4k9v1q9r0g");
        System.out.println(res);
    }



    public static String replaceDigits1(String s){
        StringBuilder builder = new StringBuilder();
        for (int i=0;i<s.length()-1;i+=2){
            builder.append(s.charAt(i)).append((char) (s.charAt(i)+s.charAt(i+1)-'0'));
        }
        System.out.println(builder);
        if (s.length()%2==1){
            builder.append(s.charAt(s.length()-1));
        }
        return builder.toString();
    }

    public static String replaceDigits(String s) {
        StringBuilder builder = new StringBuilder(s.length());
        char[] arr = s.toCharArray();
        for (int i = 0; i < arr.length; i += 2) {
            builder.append(arr[i]);
            if (i + 1 < arr.length) {
                char res = shift(arr[i],  arr[i + 1]);
                builder.append(res);
            }
        }
        return builder.toString();
    }

    private static Character shift(char ch, int index) {
        Map<Integer, Character> map = new HashMap<>();
        for (char i = 'a'; i <='z'; i++) {
            map.put((int) i, i);
        }
        return map.get(ch + index - '0');
    }
}
