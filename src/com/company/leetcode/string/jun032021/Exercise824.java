package com.company.leetcode.string.jun032021;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Exercise824 {

    public static void main(String[] args) {
        String res = toGoalLatin2("The quick brown fox jumped over the lazy dog");
        System.out.println(res);
    }


    public static String toGoatLatin(String sentence) {
        Set<Character> set = Set.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

        String[] arr = sentence.split(" ");
        StringBuilder builder = new StringBuilder();
        int count = 1;
        for (String word : arr) {
            System.out.println(word);
            int index = 0;
            if (!set.contains(word.charAt(0))) {
                builder.append(word.substring(1)).append(word.charAt(0));
            } else {
                builder.append(word);
            }

            builder.append("ma");
            while (index < count) {
                builder.append("a");
                index++;
            }

            count++;

            builder.append(" ");

        }
        return builder.toString().trim();
    }

    public static String toGoalLatin2(String sentence) {
        Set<Character> vowels = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
        String res = "";
        int i=0, j;
        for (String word : sentence.split("\\s")) {
            res += ' ' + (vowels.contains(word.charAt(0)) ? word : word.substring(1) + word.charAt(0)) + "ma";
            for (j = 0, ++i; j < i; ++j) {
                res += "a";
            }
        }
        return res.substring(1);
    }
}
