package com.company.leetcode.string.jun032021;

import java.util.HashSet;
import java.util.Set;

public class Exercise1704 {

    public static void main(String[] args) {
        boolean res = halvesAreAlike("AbCdEfGh");
        System.out.println(res);
    }

    public static boolean halvesAreAlike(String s) {
        Set<Character> set = Set.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');

        int countA = 0;
        int countB = 0;
        int len = s.length() / 2;

        for (int i = 0; i < len; i++) {
            if (set.contains(s.charAt(i)))
                countA++;
        }

        for (int i = len; i < s.length(); i++) {
            if (set.contains(s.charAt(i)))
                countB++;
        }

        return countA == countB;
    }
}
