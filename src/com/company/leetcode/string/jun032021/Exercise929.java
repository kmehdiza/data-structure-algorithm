package com.company.leetcode.string.jun032021;

import java.util.HashSet;
import java.util.Set;

public class Exercise929 {

    public static void main(String[] args) {
        String[] arr = {"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"};
        int res = numUniqueEmails(arr);
        System.out.println(res);
    }

    public static int numUniqueEmails(String[] emails) {
        Set<String> set = new HashSet<>();
        for (String email : emails) {
            StringBuilder builder = new StringBuilder();
            int index = 0;
            while (index < email.indexOf('@')) {
                if (email.charAt(index) != '.' && email.charAt(index) != '+') {
                    builder.append(email.charAt(index));
                }

                if (email.charAt(index) == '+') {
                    break;
                }

                index++;
            }
            builder.append(email.substring(email.indexOf('@')));
            set.add(builder.toString());
        }

        return set.size();
    }

    public static int numUniqueEmails1(String[] emails){
        Set<String> set = new HashSet<>();
        for (String email:emails){
            String[] parts = email.split("@");
            String[] local = parts[0].split("\\+");
            set.add(local[0].replace(".","")+"@"+parts[1]);
        }
        return set.size();
    }


}
