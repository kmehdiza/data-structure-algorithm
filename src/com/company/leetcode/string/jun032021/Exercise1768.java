package com.company.leetcode.string.jun032021;

public class Exercise1768 {

    public static void main(String[] args) {
        //word1 = "abc", word2 = "pqr"
        //word1 = "ab", word2 = "pqrs"
        mergeAlternately("cdf", "a");
    }

    public static String mergeAlternately(String word1, String word2) {
        StringBuilder builder = new StringBuilder();
        int i=0, j = 0;
        int len1 = word1.length();
        int len2 = word2.length();
        while (i < len1 || j < len2) {
            if (i<len1){
                builder.append(word1.charAt(i++));
            }
            if (j<len2){
                builder.append(word2.charAt(j++));
            }
        }

        return builder.toString();
    }

}
