package com.company.leetcode.string.jun032021;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Exercise1436 {

    public static void main(String[] args) {
        List<List<String >> paths = List.of(List.of("A","Z"));
        String res = destCity(paths);
        System.out.println(res);
    }

    public static String destCity(List<List<String>> paths){
        Set<String> set = new HashSet<>();

        for (List<String> path:paths) set.add(path.get(1));
        for (List<String> path:paths) set.remove(path.get(0));

        return set.iterator().next();
    }


}
