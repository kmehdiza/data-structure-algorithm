package com.company.leetcode.string.jun032021;

public class Exercise1816 {

    public static void main(String[] args) {
        String res = truncateSentences("Hello how are you Contestant", 4);
        System.out.println(res);
    }

    public static String truncateSentences(String s, int k) {
        StringBuilder builder = new StringBuilder(s.length());
        String[] arr = s.split(" ");
        int count = 0;
        while (count < k - 1) {
            builder.append(arr[count]).append(" ");
            count++;
        }
        builder.append(arr[k - 1]);
        return builder.toString();
    }

    public static String truncateSentences1(String s, int k) {
        int count = 0;
        int spaceCount = 0;
        while (count < s.length() && spaceCount < k) {
            if (s.charAt(count) == ' ')
                spaceCount++;
            count++;
        }
        return spaceCount == k ? s.substring(0, s.charAt(count - 1)) : s;
    }

}
