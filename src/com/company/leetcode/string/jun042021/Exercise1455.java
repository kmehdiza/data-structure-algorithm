package com.company.leetcode.string.jun042021;

public class Exercise1455 {

    public static void main(String[] args) {
        int res = isPrefixOfWord("i use triple pillow","pill");
        System.out.println(res);
    }

    // sentence = "i love eating burger", searchWord = "burg"
    public static int isPrefixOfWord(String sentence, String searchWord) {
        String[] words = sentence.split(" ");
        int index = 1;
        for (String word : words) {
            if (word.contains(searchWord) && word.substring(0,searchWord.length()).equals(searchWord)){
                return index;
            }
            index++;
        }

        return -1;
    }

}
