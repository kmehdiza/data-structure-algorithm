package com.company.leetcode.string.jun042021;

import java.util.HashMap;
import java.util.Map;

public class Exercise1876 {


    public static void main(String[] args) {
        int res = countGoodSubstrings("xyzzaz");
        System.out.println(res);
    }

    // xyzzaz
    public static int countGoodSubstrings(String s){
        int count = 0;
        for (int i=0; i<s.length()-2;i++){
            String subString = s.substring(i, i+3);
            if (checkGoodSubstring(subString))
                count++;
        }
        return count;
    }

    private static boolean checkGoodSubstring(String word){
        Map<Integer,Integer> map = new HashMap<>();
        for (int i=0;i<word.length();i++){
            map.put((int)word.charAt(i),map.getOrDefault((int)word.charAt(i),0)+1);
        }
        int index = 0;
        while (index<map.size()){
           if (map.get((int)word.charAt(index))>1){
               return false;
           }
            index++;
        }

        return true;
    }

}
