package com.company.leetcode.string.jun042021;

import java.util.HashMap;
import java.util.Map;

public class Exercise1507 {


    public static void main(String[] args) {
        reformatDate("20th Oct 2052");
    }

    public static String reformatDate(String date) {
        String[] month = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < month.length; i++) {
            map.put(month[i], (i + 1 < 10 ? "0" : "") + (i + 1));
        }

        StringBuilder builder = new StringBuilder();
        System.out.println(map);
        //"2052-10-20"
        String[] dateArr = date.split(" ");

        System.out.println(dateArr[0].substring(0,dateArr[0].length()-2));

        builder.append(dateArr[2]+"-").append(map.get(dateArr[1])+"-").append((dateArr[0].length()==3?"0":"")+dateArr[0].substring(0,dateArr[0].length()-2));

        System.out.println(builder);
        return builder.toString();
    }


}
