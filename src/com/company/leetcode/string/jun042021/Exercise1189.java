package com.company.leetcode.string.jun042021;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Exercise1189 {

    public static void main(String[] args) {
        maxNumberOfBalloons("loonbalxballpoon");
    }

    public static int maxNumberOfBalloons(String text) {
        StringBuilder builder = new StringBuilder();
        String balloon = "balloon";
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < text.length(); i++) {
            if (balloon.indexOf(text.charAt(i)) != -1) {
                System.out.println(text.charAt(i));
                builder.append(text.charAt(i));
            }
        }

        System.out.println(min);
        return 0;
    }

    public static int maxNumberOfBalloons1(String text){

        int[] cnt = new int[26];
        int[] cntBalloon = new int[26];
        int min = Integer.MAX_VALUE;

        for (int i=0; i<text.length();i++){
            ++cnt[text.charAt(i)-'a'];
        }

        for (char c:"balloon".toCharArray()){
            ++cntBalloon[c-'a'];
        }

        for (char c:"balloon".toCharArray()){

            min = Math.min(min,cnt[c-'a']/cntBalloon[c-'a']);
        }
        return min;
    }
}
