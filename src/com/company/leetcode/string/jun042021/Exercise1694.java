package com.company.leetcode.string.jun042021;

public class Exercise1694 {


    public static void main(String[] args) {
       String res =  reformatString("123 4-5678");
        System.out.println(res);
    }

    public static String reformatString(String number) {
        StringBuilder builder = new StringBuilder(number.length());
        for (int i = 0; i < number.length(); i++) {
            if (number.charAt(i) != ' ' && number.charAt(i) != '-') {
                builder.append(number.charAt(i));
            }
        }

        //123456
        int index = 0;
        while (index<builder.length()-4){
            builder.insert(index+3,"-");
            index+=4;
        }

        if (builder.length()-index==4){
            builder.insert(index+2,"-");
        }

        return builder.toString();
    }

    public String reformatNumber1(String number) {
        StringBuilder sb = new StringBuilder();
        for (char c : number.toCharArray()) {
            if (Character.isDigit(c)) sb.append(c);
        }
        int i = 0;
        while(i < sb.length()-4) {
            sb.insert(i+3, '-');
            i+=4;
        }

        if (sb.length() - i == 4) {
            sb.insert(i+2, '-');
        }
        return sb.toString();
    }
}
