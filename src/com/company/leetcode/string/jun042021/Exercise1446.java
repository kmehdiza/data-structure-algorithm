package com.company.leetcode.string.jun042021;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Exercise1446 {

    public static void main(String[] args) {
        int res = maxPower1("ccbccbb");
        System.out.println(res);
    }

    //this did not work in this input: ccbccbb
    public static int maxPower(String s) {
        Map<Character, Integer> map = new HashMap<>();
        Integer max = Integer.MIN_VALUE;
        for (int i = 0; i < s.length(); i++) {
            if (i + 1 < s.length() && s.charAt(i) == s.charAt(i + 1)) {
                map.put(s.charAt(i), map.getOrDefault(s.charAt(i), 1) + 1);
            }
        }

        for (Map.Entry<Character, Integer> m : map.entrySet()) {
            max = Math.max(max, m.getValue());
        }
        if (map.isEmpty()) {
            return 1;
        }
        return max;
    }

    public static int maxPower1(String s) {
        int count = 0;
        int max = 0;
        char previous = ' ';
        for (int i = 0; i < s.length(); i++) {
            char current = s.charAt(i);
            if (current == previous) {
                count++;
            } else {
                count = 1;
                previous = current;
            }
            System.out.println(current+": "+count);
            max = Math.max(max, count);
        }
        return max;
    }
}
