package com.company.leetcode.string.jun042021;

import java.util.*;

public class Exercise1408 {


    public static void main(String[] args) {
        String[] words = {"leetcoder", "leetcode", "od", "hamlet", "am"};
        List<String> list = stringMatching2(words);
        System.out.println(list);
    }

    public static List<String> stringMatching(String[] words) {

        Arrays.sort(words, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.length() - s2.length();
            }
        });

        Arrays.stream(words).forEach(System.out::println);

        List<String> list = new LinkedList<>();

        for (int i = 0; i < words.length; i++) {
            int j = i + 1;
            while (j < words.length) {
                System.out.println("j: " + words[j]);
                System.out.println("i: " + words[i]);
                int index = words[j++].indexOf(words[i]);
                if (index != -1) {
                    list.add(words[i]);
                    break;
                }
            }
        }
        return list;
    }
//{"leetcoder", "leetcode", "od", "hamlet", "am"};
    public static List<String> stringMatching2(String[] words) {

        HashSet<String> set = new HashSet<>();

        for (int i = 0; i < words.length-1; i++) {
            String currentWord = words[i];
            System.out.println("currentWord: "+currentWord);
            for (int j = i + 1; j < words.length; j++) {
                String nextWord = words[j];
                System.out.println("nextWord: "+nextWord);
                if (currentWord.contains(nextWord)) {
                    set.add(nextWord);
                }

                if (nextWord.contains(currentWord)) {
                    set.add(currentWord);
                }
            }
        }
        List<String> list = new ArrayList<>(set);
        return list;

    }

}
