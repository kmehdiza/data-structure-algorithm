package com.company.leetcode.string.jun092021;

public class Exercise1556 {

    public static void main(String[] args) {
        thousandSeparator(123456789);
    }

    public static String thousandSeparator(int n){
        String num = Integer.toString(n);
        StringBuilder builder = new StringBuilder(num);
        if (num.length()<=3)
            return num;

        for (int i=num.length()-3; i>0;i-=3){
            System.out.println(i);
            builder.insert(i,'.');
        }
        System.out.println(builder);
        return builder.toString();
    }


}
