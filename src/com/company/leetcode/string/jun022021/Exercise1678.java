package com.company.leetcode.string.jun022021;

public class Exercise1678 {

    public static void main(String[] args) {
        interpret("G()()()()(al)");
    }

    /*
     * G -> G
     * () -> O
     * (al) -> al
     * */

    // i=0 c = g; i=1,c=(,i=2; i=3,
    public static String interpret(String command) {
        StringBuilder builder = new StringBuilder(command.length());
        System.out.println(command.length());
        for (int i=0; i<command.length();i++){
            System.out.println("i:"+i);
            System.out.println("ch: "+command.charAt(i));
            if (command.charAt(i)=='G'){
                builder.append("G");
            }else if(command.charAt(i)=='('){
                if(command.charAt(i+1)==')'){
                    builder.append("0");
                }else {
                    builder.append("al");
                }
            }
        }
        return builder.toString();
    }
}
