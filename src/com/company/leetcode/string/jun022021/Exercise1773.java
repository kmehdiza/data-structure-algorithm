package com.company.leetcode.string.jun022021;

import java.lang.reflect.Array;
import java.util.*;

public class Exercise1773 {

    public static void main(String[] args) {
        List<List<String>> list = new ArrayList<>();
        List<String> innerList = new ArrayList<>();
        innerList.add("phone");
        innerList.add("blue");
        innerList.add("pixel");
        list.add(innerList);

        List<String> innerList1 = new ArrayList<>();
        innerList1.add("computer");
        innerList1.add("silver");
        innerList1.add("lenevo");

        list.add(innerList1);

        List<String> innerList2 = new ArrayList<>();
        innerList2.add("phone");
        innerList2.add("gold");
        innerList2.add("iphone");

        list.add(innerList2);

        countMatches(list, "color", "silver");
    }

    public static int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        Map<String, Integer> map = new HashMap<>();
        int count = 0;
        map.put("type", 0);
        map.put("color", 1);
        map.put("name", 2);

        for (List<String> item : items) {
            if (map.containsKey(ruleKey)) {
                if (item.get(map.get(ruleKey)).equals(ruleValue)) {
                    count++;
                }
            }
        }
        return count;
    }

    public static int countMatchesWithLambda(List<List<String>> items, String ruleKey, String ruleValue) {
        Map<String, Integer> map = Map.of("type", 0, "color", 1, "name", 2);
        return (int) items.stream().filter(item -> item.get(map.get(ruleKey)).equals(ruleValue)).count();
    }

}
