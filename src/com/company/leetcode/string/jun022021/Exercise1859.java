package com.company.leetcode.string.jun022021;

import java.util.HashMap;
import java.util.Map;

public class Exercise1859 {


    public static void main(String[] args) {
        System.out.println(sortSentence("is2 sentence4 This1 a3"));
    }

    public static String sortSentence(String s) {
        Map<Integer, String> map = new HashMap<>();
        StringBuilder builder = new StringBuilder(s.length());
        String[] arr = s.split(" ");
        for (int i = 0; i < arr.length; i++) {
            String word = arr[i];
            map.put(Integer.parseInt(word.substring(word.length() - 1)), word.substring(0, word.length() - 1));
        }
        int count = 1;
        while (!map.isEmpty()) {
            builder.append(map.get(count));
            if (map.size()>count){
                builder.append(" ");
            }
            count++;
            if (map.size() < count) {
                break;
            }
        }
        return builder.toString();
    }
}
