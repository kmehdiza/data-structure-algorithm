package com.company.leetcode.string.jun022021;

import java.util.HashSet;
import java.util.Set;

public class Exercise1832 {

    public static void main(String[] args) {
        boolean res = checkIfPangram2("leetcode");
        System.out.println(res);
    }

    public static boolean checkIfPangram(String sentence) {
        char[] arr = sentence.toCharArray();
        Set<Character> set = new HashSet<Character>();
        for (char letter = 'a'; letter <= 'z'; letter++) {
            set.add(letter);
        }

        for (int i = 0; i < arr.length; i++) {
            if (set.contains(arr[i])) {
                set.remove(arr[i]);
            }
        }
        if (!set.isEmpty()) {
            return false;
        }
        return true;
    }

    public static boolean checkIfPangram2(String sentence) {

        boolean[] letters = new boolean[26];
        for (char c : sentence.toCharArray()) {
            int i = c-'a';
            System.out.println(i);
            letters[i] = true;
        }

        for (boolean l:letters){
            System.out.print(l);
        }

        for (boolean existLetter : letters) {
            if (!existLetter) {
                return false;
            }
        }

        return true;
    }
}
