package com.company.leetcode.string.jun022021;

public class Exercise1684 {


    public static void main(String[] args) {
        String[] arr = {"ad","bd","aaab","baa","badab"};
        countConsistentStrings("ab",arr);
    }

    public static int countConsistentStrings(String allowed, String[] words) {
        int count = 0;
        for (String word : words) {
            boolean isValid = true;
            for (char letter : word.toCharArray()) {
                System.out.println(String.valueOf(letter));
                System.out.println(allowed.contains(String.valueOf(letter)));
                if (!allowed.contains(String.valueOf(letter))) {
                    isValid = false;
                }
            }
            if (isValid) {
                count++;
            }
        }
        return count;
    }

    public static int countConsistentStrings2(String allowed, String[] words) {
        int count = 0;
        boolean[] boolArr = new boolean[26];
        for (char allowLetter : allowed.toCharArray()) {
            boolArr[allowLetter - 'a'] = true;
        }

        for (String word : words) {
            boolean allow = true;
            for (char letter : word.toCharArray()) {
                if (!boolArr[letter - 'a']) {
                    allow = false;
                    break;
                }
            }
            if (allow) {
                count++;
            }
        }

        return count;
    }

}
