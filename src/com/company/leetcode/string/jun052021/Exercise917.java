package com.company.leetcode.string.jun052021;

import java.util.Arrays;

public class Exercise917 {

    public static void main(String[] args) {
        reverseOnlyLetters("a-bC-dEf-ghIj");
    }

    //"j-Ih-gfE-dCba"
    public static String reverseOnlyLetters(String s) {
        int beginIndex = 0;
        int lastIndex = s.length() - 1;
        char[] arr = s.toCharArray();
        while (beginIndex < lastIndex) {

            if (Character.isLetter(arr[beginIndex]) && Character.isLetter(arr[lastIndex])) {
                char temp = arr[beginIndex];
                arr[beginIndex] = arr[lastIndex];
                arr[lastIndex] = temp;
                beginIndex++;
                lastIndex--;
            }

            if (!Character.isLetter(arr[beginIndex]))
                beginIndex++;

            if (!Character.isLetter(arr[lastIndex]))
                lastIndex--;
        }

        return new String(arr);
    }
}
