package com.company.leetcode.string.jun052021;

public class Exercise1624 {


    public static void main(String[] args) {
        System.out.println(maxLengthBetweenEqualCharacters("abca"));
    }

    //"abca"
    public static int maxLengthBetweenEqualCharacters(String s) {
        int max = -1;

        if (s.length() ==1)
            return -1;

        if (s.length() == 2 && s.charAt(0) == s.charAt(1))
            return 0;

        for (int i = 0; i < s.length(); i++) {
            int lastIndex = s.lastIndexOf(s.charAt(i));
            System.out.println(lastIndex);
            if (i != lastIndex) {
                max = Math.max(max, (lastIndex - i - 1));
            }
        }
        return max;
    }
}
