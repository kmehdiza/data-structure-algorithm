package com.company.leetcode;


public class Exercise1622 {

    public static void main(String[] args) {
        String[] word1 = {"ab", "c"};
        String[] word2 = {"a", "bc"};
        System.out.println(arrayStringEqual(word1, word2));
    }

    public static boolean arrayStringEqual(String[] word1, String[] word2) {
        StringBuilder builder1 = new StringBuilder();
        StringBuilder builder2 = new StringBuilder();
        for (String i : word1)
            builder1.append(i);
        for (String i : word2)
            builder2.append(i);
        return builder1.toString().equals(builder2.toString());
    }
}
