package com.company.leetcode.array;

import java.util.Arrays;

public class Exercise832 {

    public static void main(String[] args) {
        int[][] arr = {{1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
        int[][] res = flipAndInvertImage(arr);
        System.out.println(Arrays.deepToString(res));
    }

    public static int[][] flipAndInvertImage(int[][] A) {

        for (int[] row : A) {
            for (int i = 0, j = row.length - 1; i <= j; i++, j--) { //{1,1,0}
                int temp = row[j];
                row[j] = 1-row[i];
                row[i] = 1-temp;
            }
        }
        return A;
    }
}
