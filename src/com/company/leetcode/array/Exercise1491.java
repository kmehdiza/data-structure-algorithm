package com.company.leetcode.array;

public class Exercise1491 {

    public static void main(String[] args) {
        int[] arr = {4000, 3000, 1000, 2000};
        System.out.println(average(arr));
    }

    public static double average(int[] salary) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int n = salary.length;
        int sum = 0;
        for (int num : salary) {
            sum += num;
            min = Math.min(num, min);
            max = Math.max(num, max);
        }
        return (sum-max - min) / (n-2);
    }

}
