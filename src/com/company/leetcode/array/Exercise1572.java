package com.company.leetcode.array;

import java.util.Arrays;

public class Exercise1572 {

    public static void main(String[] args) {
        int[][] mat = {{1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};
        int res = diagonalSum(mat);
        System.out.println(res);
    }

    public static int diagonalSum(int[][] sum) {
        int n = sum.length, res = 0;
        for (int i = 0; i < n; i++) {
            res += sum[i][i];
            if (i != n - 1 - i) {
                res += sum[i][n - 1 - i];
            }
        }
        return res;
    }

}
