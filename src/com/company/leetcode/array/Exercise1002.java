package com.company.leetcode.array;

import java.util.*;

public class Exercise1002 {

    public static void main(String[] args) {
        String[] A = {"cool", "lock", "cook"};
        List<String> res = commonChars(A);
        System.out.println(res);
    }

    public static List<String> commonChars(String[] A) {

        HashMap<Character, Integer> commonMap = new HashMap<>();
        List<String> list = new ArrayList<>();

        for (Character i : A[0].toCharArray()) {
            commonMap.put(i, commonMap.getOrDefault(i, 0) + 1); // {c=1, l=1, o=2}
        }

        for (int i=1; i<A.length;i++){
            HashMap<Character,Integer> currentMap = new HashMap<>();
            for (Character letter:A[i].toCharArray()){
                currentMap.put(letter,currentMap.getOrDefault(letter,0)+1);
            }
            for (Character c: A[i].toCharArray()){ // {c=1, k=1, l=1, o=1}; {c=1, k=1, o=2}
                if (commonMap.containsKey(c) && (currentMap.get(c)<commonMap.get(c))){
                    commonMap.put(c,currentMap.get(c));
                }

            }
        }

        for (Character x: commonMap.keySet()){
            while (commonMap.get(x)!=0){
                list.add(String.valueOf(x));
                commonMap.put(x,commonMap.get(x)-1);
            }
        }

        return list;
    }

}
