package com.company.leetcode.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercise1200 {


    public static void main(String[] args){
        int[] arr = {4,2,1,3};
        minimumAbsDifference(arr);
    }

    public static List<List<Integer>>  minimumAbsDifference(int[] arr){
        Arrays.sort(arr);
        List<List<Integer>> list = new ArrayList<>();
        int min = Integer.MAX_VALUE;
        for (int i=1; i<arr.length;i++){
            min = Math.min(min, arr[i]-arr[i-1]);
        }

        for (int i=1; i<arr.length;i++){
            if (arr[i]-arr[i-1]==min) list.add(List.of(arr[i],arr[i-1]));
        }

        return list;
    }

}
