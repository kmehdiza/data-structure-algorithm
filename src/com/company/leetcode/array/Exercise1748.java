package com.company.leetcode.array;

import java.util.*;

public class Exercise1748 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2};
        System.out.println(sumOfUnique(arr));
    }

    public static int sumOfUnique(int[] nums) {
        int sum = 0;
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
        }
        for (int num : nums) {
            if (map.get(num) == 1) {
                sum += num;
            }
        }
        return sum;
    }

}
