package com.company.leetcode.array;

import java.util.Arrays;
import java.util.Stack;

public class Exercise1475 {

    public static void main(String[] args) {
        int[] arr = {8, 4, 6, 2, 3};
        int[] res = finalPrices(arr);
        Arrays.stream(res).forEach(System.out::println);

    }

    public static int[] finalPrices(int[] prices) {
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < prices.length; i++) {
            while (!stack.isEmpty() && prices[stack.peek()] >= prices[i]) {
                prices[stack.pop()]-=prices[i];
            }
            stack.push(i);
        }
        return prices;
    }


}
