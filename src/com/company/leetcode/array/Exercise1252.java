package com.company.leetcode.array;

import java.util.Arrays;

public class Exercise1252 {

    public static void main(String[] args) {
        int n = 2, m = 3;
        int[][] indices = {{0, 1}, {1, 1}};
        int res = oddCells(n, m, indices);
        System.out.println(res);
    }

    public static int oddCells(int n, int m, int[][] indices) {
        int count = 0;
        int[] row = new int[n];// 0 0
        int[] col = new int[m]; // 0 0 0

        for (int[] coordinate : indices) {
            row[coordinate[0]]++; // 1, 1
            col[coordinate[1]]++; // 0, 2, 0
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if ((row[i] + col[j]) % 2 != 0){
                    count++;
                }
            }
        }

        return count;
    }
}
