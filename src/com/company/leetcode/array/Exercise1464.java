package com.company.leetcode.array;

public class Exercise1464 {

    public static void main(String[] args) {
        int[] arr = {3, 4, 5, 2};
        System.out.println(maxProduct(arr));
    }

    public static int maxProduct(int[] nums) {
        int max1 = -1;
        int max2 = -1;

        for (int num : nums) { // num = 3, num = 4, num = 5; num = 2
            if (num >= max2) {
                max1 = max2; // max1 = -1; max1 = 3
                max2 = num; // max2 = 3;  max2 = 4
            } else if (num > max1) {
                max1 = num; // max1 = 5
            }
        }
        return --max1 * --max2;
    }
}
