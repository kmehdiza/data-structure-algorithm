package com.company.leetcode.array;

import java.util.*;

public class Exercise1700 {

    public static void main(String[] args) {
        int[] students = {1, 1, 0, 0};
        int[] sandwich = {0, 1, 0, 1};
        System.out.println(countStudents(students, sandwich));
    }

    public static int countStudents(int[] students, int[] sandwiches) {
        List<Integer> student = new ArrayList<>();
        List<Integer> sandwich = new ArrayList<>();

        for (int i:students)
            student.add(i);

        for (int i: sandwiches)
            sandwich.add(i);

        int count = 0;
        while (sandwich.size()>0){
            if (student.get(0).equals(sandwich.get(0))){
                student.remove(0);
                sandwich.remove(0);
                count = 0;
            }else {
                student.add(student.get(0));
                student.remove(0);
                count++;
                if (student.size()==count) return count;
            }

        }

        return count;
    }
}
