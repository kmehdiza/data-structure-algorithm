package com.company.leetcode.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Exercise1672 {

    public static void main(String[] args) {
        int[][] accounts = {{1, 5}, {7, 3}, {3, 5}};
        System.out.println(maximumWealth2(accounts));
    }

    public static int maximumWealth(int[][] accounts) {
        List<Integer> list = new ArrayList<>();
        for (int[] account : accounts) {
            int max = 0;
            for (int num : account) {
                max += num;
            }
            list.add(max);
        }
        Collections.sort(list);
        return list.get(list.size() - 1);
    }

    public static int maximumWealth2(int[][] accounts) {
        int max = 0;
        for (int[] account : accounts) {
            int res = 0;
            for (int num : account) {
                res += num;
            }
            max = Math.max(res, max);
        }
        return max;
    }
}
