package com.company.leetcode.array;

import java.util.ArrayList;
import java.util.List;

public class Exercise1732 {

    public static void main(String[] args) {
        int[] arr = {-4,-3,-2,-1,4,3,2};
        System.out.println(largestAltitude2(arr));
    }

    public static int largestAltitude(int[] gain) {
        int max = 0;
        int point = 0;
        List<Integer> list = new ArrayList<>();
        list.add(0);
        for (int i = 0; i < gain.length; i++) {
            point += gain[i];
            list.add(point);
        }
        for (int i:list){
            max = Math.max(max, i);
        }
        return max;
    }

    public static int largestAltitude2(int[] gain){
        int max = 0;
        int point = 0;

        for (int i:gain){
            point +=i;
            max = Math.max(max,point);
        }
        return max;
    }
}
