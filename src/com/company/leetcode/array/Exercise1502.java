package com.company.leetcode.array;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Exercise1502 {


    public static void main(String[] args) {
        int[] arr = {3, 5, 1};
        System.out.println(canMakeArithmeticProgression2(arr));
    }

    public static boolean canMakeArithmeticProgression(int[] arr) {

        Arrays.sort(arr); // 1,3,5
        for (int i = 2; i < arr.length; i++) {
            if (arr[i] - arr[i - 1] != arr[i - 1] - arr[i - 2]) return false;
            //  5    -    3            3      -     1
        }
        return true;
    }

    public static boolean canMakeArithmeticProgression2(int[] arr) {
        int min = -1;
        int max = -1;
        int n = arr.length;
        for (int num : arr) {
            min = Math.min(min, num);
            System.out.println("Min " + min);
            max = Math.max(max, num);
            System.out.println("max " + max);
        }

        if ((max - min) % (n - 1) != 0) return false;
        int step = (max-min) / (n-1);
        if (step==0) return true;
        Set<Integer> set = new HashSet<>();
        for (int num : arr) {
            if ((num-min)%step!=0) return false;
            if (!set.add(num)) return false;
        }
        return true;
    }
}
