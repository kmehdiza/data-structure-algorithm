package com.company.leetcode.array;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Exercise1460 {

    public static void main(String[] args){
        int[] target = {1,2,3,4};
        int[] arr = {2,4,1,3};
        System.out.println(canBeEqual(target, arr));
    }

    public static boolean canBeEqual(int[] target, int[] arr){
        int t = target.length, a = arr.length;
        if (a!=t){
            return false;
        }

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i<a;i++){
            map.put(target[i],map.getOrDefault(target[i],0)+1);
            map.put(arr[i],map.getOrDefault(arr[i],0)-1);
        }

        System.out.println(map);

        for (int i: map.keySet()){
            if (map.get(i)!=0) return false;
        }

        return true;
    }

}
