package com.company.sort;

import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {
        int[] arr = {10, 9, 7, 101, 23, 44, 12, 78, 34, 23};
        int[] res = sortArray(arr);
        for (int i : res) {
            System.out.print(i + ", ");
        }
    }
//{101, 9, 7, 10, 23, 44, 12, 78, 34, 23}
    public static int[] sortArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
            for (int j = 0; j < arr.length; j++) {
                if (arr[i] < arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            System.out.println(" ");
            Arrays.stream(arr).forEach(System.out::println);
            System.out.println(" ");
        }
        return arr;
    }
}
