package com.company.grokking.algorithms.chapter2;

import java.util.Arrays;

public class SelectionSortArray {

    public static void main(String[] args) {
        int[] arr = {5, 3, 6, 2, 10};
        int[] res = selectionSort(arr);
        Arrays.stream(res).forEach(System.out::println);

    }

    public static int[] selectionSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
        return arr;
    }

}
