package com.company.grokking.algorithms.chapter2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectionSortList {

    public static void main(String[] args) {
        List<Integer> arr = new ArrayList<>(Arrays.asList(5, 3, 6, 2, 10));
        System.out.println(selectionSort(arr));
    }

    public static int findSmallest(List<Integer> arr) {
        int minVal, minIndex = 0;
        minVal = arr.get(0);
        for (int i = 1; i < arr.size(); i++) {
            if (arr.get(i) < minVal) {
                minVal = arr.get(i);
                minIndex = i;
            }
        }
        return minIndex;
    }

    public static List<Integer> selectionSort(List<Integer> arr){
        List<Integer> newArr = new ArrayList<>(arr.size());
        int size = arr.size();
        for (int i=0; i<size;i++){
            int smallIndex = findSmallest(arr);
            newArr.add(arr.get(smallIndex));
            arr.remove(smallIndex);
        }
        return newArr;
    }

}
