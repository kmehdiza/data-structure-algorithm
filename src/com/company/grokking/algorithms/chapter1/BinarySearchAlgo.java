package com.company.grokking.algorithms.chapter1;

import java.util.Arrays;

public class BinarySearchAlgo {

    public static void main(String[] args) {
        int[] arr = {1, 5, 2, 10, 23, 32, 11};
        System.out.println(binarySearch(arr, 111));
    }

    public static int binarySearch(int[] arr, int num) {
        Arrays.sort(arr);
        Arrays.stream(arr).forEach(System.out::println);
        int low, high, mid;
        low = 0;
        high = arr.length - 1;
        while (low <= high) {
            mid = (high + low) / 2;
            if (arr[mid] == num) {
                return mid;
            } else if (arr[mid] < num) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}
