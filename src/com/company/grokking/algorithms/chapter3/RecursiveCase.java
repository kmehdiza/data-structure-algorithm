package com.company.grokking.algorithms.chapter3;

public class RecursiveCase {

    public static void main(String[] args) {
        recursiveFunc(5);
    }

    public static void recursiveFunc(int i) {
        System.out.println(i);
        if (i < 0) {
            return;
        } else
            recursiveFunc(i - 1);
    }
}
