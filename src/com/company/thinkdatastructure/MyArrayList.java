package com.company.thinkdatastructure;

public class MyArrayList<E> {

    private int size;
    private E[] array;

    public E getList(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    public E setList(int index, E element) {
        E oldElement = getList(index);
        array[index] = oldElement;
        return oldElement;
    }
}
