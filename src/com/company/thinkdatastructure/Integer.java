package com.company.thinkdatastructure;

import java.util.ArrayList;
import java.util.List;

public final class Integer extends Number implements Comparable<java.lang.Integer> {

    @Override
    public int compareTo(java.lang.Integer integer) {
        return 0;
    }

    @Override
    public int intValue() {
        return 0;
    }

    @Override
    public long longValue() {
        return 0;
    }

    @Override
    public float floatValue() {
        return 0;
    }

    @Override
    public double doubleValue() {
        return 0;
    }
}
