package com.company.interview;

import java.util.List;

public class Exercise {

    public static void main(String[] args){
        List<Integer> list = List.of(3,1,2,3);
        System.out.println(reductionCost(list));
    }

    /*
3
1
2
3
     */
        public static int reductionCost(List<Integer> num) {
            System.out.println(num);
            // Write your code here
            int sum =0;
            boolean result = true;
            while(result){ // 3, 1, 2, 3 , 4
                sum = num.get(0)+num.get(1); // 4
                num.add(sum);
                if(num.size()!=1){
                    num.remove(0);
                    num.remove(1);
                }

                if(num.size()==1){
                    result = false;
                }
            }
            return-1;
    }

}
