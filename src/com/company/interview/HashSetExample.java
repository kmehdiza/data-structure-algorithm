package com.company.interview;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetExample {

    public static void main(String[] args) {
        Set<String> streams = new HashSet<>();
        streams.add("Programming");
        streams.add("Development");
        streams.add("Debugging");

        Iterator<String> itr = streams.iterator();
        while (itr.hasNext()) {
            System.out.println("Iterating over hashset in Java current object:" + itr.next());
        }

    }


}
